<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use MohamedGaber\SanctumRefreshToken\Traits\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, HasRoles, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'mobile'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function productsCount()
    {
        return count($this->products);
    }

    public function hasProducts()
    {
        return count($this->products) > 0;
    }

    public function activeProducts()
    {
        return $this->products->where('active', 1);
    }

    public function activeProductsCount()
    {
        return count($this->products->where('active', 1));
    }

    public function hasActiveProducts()
    {
        return count($this->products->where('active', 1)) > 0;
    }

    public function productsWithStock()
    {
        return $this->products->where('stock', '>', 1);
    }

    public function productsWithStockCount()
    {
        return count($this->products->where('stock', '>', 1));
    }

    public function hasProductsWithStock()
    {
        return count($this->products->where('stock', '>', 1)) > 0;
    }
}
