<?php


namespace App\Traits;

use Symfony\Component\HttpFoundation\Response;

trait ResponseFactory
{
    protected function sendResponse(string $message, $data = null)
    {
        $response = [
            'success' => true,
            'data' => $data,
            'message' => $message
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    protected function sendError(string $message, int $code = Response::HTTP_NOT_FOUND, array $errorMessages = null, array $data = null)
    {
        $response = [
            'succcess' => false,
            'data' => $data,
            'message' => $message
        ];

        if ($errorMessages) $response['errors'] =  $errorMessages;

        return response()->json($response, $code);
    }
}
