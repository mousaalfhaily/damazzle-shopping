<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    use ResponseFactory;

    public function __construct()
    {
        $admin_role = config('roles.admin');
        $super_admin_role = config('roles.super_admin');
        $this->middleware("role:$admin_role|$super_admin_role,sanctum");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(25);
        return $this->sendResponse(__('user.users_list'), $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->user()->hasRole('super_admin'))
            return $this->sendError(__('auth.unauthorized'), Response::HTTP_UNAUTHORIZED);

        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|max:50|min:3',
            'username' => 'required|max:50|min:3|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'verified' => 'boolean',
            'mobile' => 'required|digits:10',
            'password' => 'required|confirmed',
            'role' => Rule::in(array_keys(config('roles')))
        ]);

        if ($validator->fails())
            return $this->sendError(__('auth.error_validation'), Response::HTTP_BAD_REQUEST, $validator->errors()->toArray());

        $input['password'] = Hash::make($input['password']);

        if (isset($input['verified']) && (bool) $input['verified']) {
            $input['email_verified_at'] = now();
        } else if (isset($input['verified']) && !(bool) $input['verified']) {
            $input['email_verified_at'] = null;
        }

        $user = User::create($input);
        !isset($input['role']) ?: $user->assignRole($input['role']);

        $result = [
            'user' => $user,
        ];

        return $this->sendResponse(__('auth.user_signed_up_successfully'), $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if (is_null($user)) return $this->sendError(__('user.user_not_found'));
        return $this->sendResponse(__('user.view_user'), $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if (!$request->user()->hasRole('super_admin'))
            return $this->sendError(__('auth.unauthorized'), Response::HTTP_UNAUTHORIZED);

        $input = $request->all();

        $validation = [
            'name' => 'max:50|min:3',
            'username' => 'max:50|min:3',
            'email' => 'email',
            'verified' => 'boolean',
            'mobile' => 'digits:10',
            'role' => Rule::in(array_keys(config('roles')))
        ];

        if (isset($input['username']) && $input['username'] != $user->username)
            $validation['username'] = 'max:50|min:3|unique:users,username';

        if (isset($input['email']) && $input['email'] != $user->email)
            $validation['email'] = 'max:50|min:3|unique:users,email';

        $validator = Validator::make($input, $validation);

        if ($validator->fails())
            return $this->sendError(__('auth.error_validation'), Response::HTTP_BAD_REQUEST, $validator->errors()->toArray());

        !isset($input['name']) ?: $user->name = $input['name'];
        !isset($input['username']) ?: $user->username = $input['username'];
        !isset($input['email']) ?: $user->email = $input['email'];
        !isset($input['mobile']) ?: $user->mobile = $input['mobile'];
        !isset($input['role']) ?: $user->syncRoles($input['role']);

        if ($input['verified'] && (bool) $input['verified']) {
            $user->email_verified_at = now();
        } else if (!(bool) $input['verified']) {
            $user->email_verified_at = null;
        }

        if ($user->save())
            return $this->sendResponse(__('user.user_updated_successfully'), $user);
        else
            return $this->sendError(__('user.user_was\'t_updated_successfully'), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request  $request)
    {
        if (!$request->user()->hasRole('super_admin'))
            return $this->sendError(__('auth.unauthorized'), Response::HTTP_UNAUTHORIZED);

        $user = User::find($id);

        if ($user) {
            if ($user->hasRole('super_admin'))
                return $this->sendError(__('user.can\'t_delete_super_admin'), Response::HTTP_BAD_REQUEST);

            $user->delete();
            return $this->sendResponse(__('user.user_deleted_successfully'));
        } else
            return $this->sendError(__('user.user_not_found'));
    }
}
