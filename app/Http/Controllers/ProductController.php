<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Traits\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    use ResponseFactory;

    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(25);
        return $this->sendResponse(__('product.products_list'), $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
            'stock' => 'required|numeric|min:1',
            'price' => 'required|numeric|min:1',
            'active' => 'boolean'
        ]);

        if ($validator->fails())
            return $this->sendError(__('product.data_validation_error'), Response::HTTP_BAD_REQUEST, $validator->errors()->toArray());

        $user = $request->user();
        $input['user_id'] = $user->id;
        $product = Product::create($input);
        $product = Product::with('user')->find($product->id);
        return $this->sendResponse(__('product.product_created_successfully'), $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if (is_null($product)) return $this->sendError(__('product.product_not_found'));
        return $this->sendResponse(__('product.view_product'), $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'stock' => 'numeric|min:1',
            'price' => 'numeric|min:1',
            'active' => 'boolean'
        ]);

        if ($validator->fails())
            return $this->sendError(__('product.data_validation_error'), Response::HTTP_BAD_REQUEST, $validator->errors()->toArray());

        !isset($input['name']) ?: $product->name = $input['name'];
        !isset($input['description']) ?: $product->description = $input['description'];
        !isset($input['image']) ?: $product->image = $input['image'];
        !isset($input['stock']) ?: $product->stock = $input['stock'];
        !isset($input['price']) ?: $product->price = $input['price'];
        !isset($input['active']) ?: $product->active = $input['active'];

        if ($product->save())
            return $this->sendResponse(__('product.product_updated_successfully'), $product);
        else
            return $this->sendError(__('product.product_was\'t_updated_successfully'), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product) {
            $product->delete();
            return $this->sendResponse(__('product.product_deleted_successfully'));
        } else
            return $this->sendError(__('product.product_not_found'));
    }
}
