<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    use ResponseFactory;

    /**
     * Sign up a new user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function signUp(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|max:50|min:3',
            'username' => 'required|max:50|min:3',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required|digits:10',
            'password' => 'required|confirmed',
        ]);

        if ($validator->fails())
            return $this->sendError(__('auth.error_validation'), Response::HTTP_BAD_REQUEST, $validator->errors()->toArray());

        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);

        $result = [
            'user' => $user,
            'token' => $user->createAuthToken($user->username)->plainTextToken,
            'refresh_token' => $user->createRefreshToken($user->username)->plainTextToken
        ];

        return $this->sendResponse(__('auth.user_signed_up_successfully'), $result);
    }

    /**
     * Sign in registered user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function signIn(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails())
            return $this->sendError(__('auth.error_validation'), Response::HTTP_BAD_REQUEST, $validator->errors()->toArray());

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = $request->user();

            $result = [
                'user' => $user,
                'token' => $user->createAuthToken($user->username)->plainTextToken,
                'refresh_token' => $user->createRefreshToken($user->username)->plainTextToken
            ];

            return $this->sendResponse(__('auth.user_signed_in_successfully'), $result);
        } else return $this->sendError(__('auth.email_or_password_are_incorrect'), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Sign out registered user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function signOut(Request $request)
    {
        $request->user()->tokens()->delete();
        return $this->sendResponse(__('auth.user_signed_out_successfully'));
    }

    /**
     * Retrieve user profile data
     *
     * @return \Illuminate\Http\Response
     */
    public function myProfile()
    {
        $result = [
            'user' => Auth::user()
        ];

        return $this->sendResponse(__('user.user_profile_info'), $result);
    }

    /**
     * Update user profile data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateMyProfile(Request $request)
    {
        $user = $request->user();
        $input = $request->all();

        $validation = [
            'name' => 'max:50|min:3',
            'username' => 'max:50|min:3',
            'email' => 'email',
            'mobile' => 'digits:10',
            'password' => 'required',
            'new_password' => 'confirmed',
        ];

        if (isset($input['username']) && $input['username'] != $user->username)
            $validation['username'] = 'max:50|min:3|unique:users,username';

        if (isset($input['email']) && $input['email'] != $user->email)
            $validation['email'] = 'max:50|min:3|unique:users,email';

        $validator = Validator::make($input, $validation);

        if ($validator->fails())
            return $this->sendError(__('auth.error_validation'), Response::HTTP_BAD_REQUEST, $validator->errors()->toArray());

        if (!Hash::check($input['password'], $user->password))
            return $this->sendError(__('auth.password_is_incorrect'), Response::HTTP_UNAUTHORIZED);

        !isset($input['name']) ?: $user->name = $input['name'];
        !isset($input['username']) ?: $user->username = $input['username'];
        !isset($input['email']) ?: $user->email = $input['email'];
        !isset($input['mobile']) ?: $user->mobile = $input['mobile'];
        !isset($input['new_password']) ?: $user->password = Hash::make($input['new_password']);

        if ($user->save())
            return $this->sendResponse(__('user.user_updated_successfully'), $user);
        else
            return $this->sendError(__('user.user_was\'t_updated_successfully'), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Get a new auth token using the refresh one
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function refreshToken(Request $request)
    {
        $user = $request->user();

        $result = [
            'token' => $user->createAuthToken($user->username)->plainTextToken,
        ];

        return $this->sendResponse(__('auth.new_token_created_successfully'), $result);
    }

    /**
     * Retrieve user profile data
     *
     * @return \Illuminate\Http\Response
     */
    public function abortForbidden()
    {
        return $this->sendError(__('auth.forbidden'), Response::HTTP_FORBIDDEN);
    }
}
