<?php

return [
    'products_permissions' => [
        'list_products',
        'store_product',
        'show_product',
        'update_product',
        'delete_product',
    ],
    'users_permissions' => [
        'list_users',
        'store_user',
        'show_user',
        'update_user',
        'delete_user',
    ],
];
