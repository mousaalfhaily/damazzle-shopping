<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name('product'),
            'description' => $this->faker->text(),
            'image' => 'https://source.unsplash.com/random/350x350/?food',
            'stock' => $this->faker->numberBetween(0, 20),
            'price' => $this->faker->numberBetween(10, 100),
            'active' => Arr::random([0, 1])
        ];
    }
}
