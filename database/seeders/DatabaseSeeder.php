<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // For live / production environment
        $this->call([
            PermissionsSeeder::class,
            RolesSeeder::class,
            RolesPermissionsSeeder::class,
            UsersSeeder::class,
        ]);

        // Only for development and testing environments
        if (App::environment('local')) {
            $this->call([
                ProductsSeeder::class,
            ]);
        }
    }
}
