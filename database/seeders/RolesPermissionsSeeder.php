<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();

        foreach ($roles as $role) {
            $permissions = [];

            switch ($role->name) {
                case 'Super Admin':
                    $permissions = [
                        config('permissions.products_permissions'),
                        config('permissions.users_permissions')
                    ];

                    break;
                case 'Admin':
                    $permissions = [
                        config('permissions.products_permissions'),
                    ];

                    break;
            }

            if (count($permissions) > 0) $role->givePermissionTo($permissions);
        }
    }
}
