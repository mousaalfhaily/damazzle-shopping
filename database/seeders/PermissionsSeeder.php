<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions_categories = config('permissions');

        foreach ($permissions_categories as $permission_category) {
            foreach ($permission_category as $permission) {
                Permission::create(['name' => $permission]);
            }
        }
    }
}
