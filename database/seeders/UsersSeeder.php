<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin Mousa',
            'username' => 'mousaalfhaily',
            'email' => config('auth.admin_email'),
            'email_verified_at' => now(),
            'password' => Hash::make(config('auth.admin_password')),
            'mobile' => '0123456789',
            'remember_token' => Str::random(10),
        ]);

        $user->assignRole('super_admin');
    }
}
