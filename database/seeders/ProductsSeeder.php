<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Product::factory()->count(5)->create();

        // OR

        $products = [
            [
                'name' => 'Milk',
                'description' => 'Dairy Product Manufacturers in Hyderabad, Telangana | Saraswati Dairy',
                'image' => 'https://pbs.twimg.com/media/Et2zm6fUYAQIn7e.jpg',
                'stock' => 10,
                'price' => 13.00,
                'active' => 1,
                'user_id' => 1
            ],
            [
                'name' => 'Coffee',
                'description' => 'Cafe Desire Granules Premium Coffee Beans',
                'image' => 'https://5.imimg.com/data5/GF/SI/SJ/SELLER-82405187/premium-coffee-beans-500x500.jpg',
                'stock' => 30,
                'price' => 18.50,
                'active' => 1,
                'user_id' => 1
            ],
            [
                'name' => 'Cookies',
                'description' => 'Multicolor Cookies Box',
                'image' => 'https://i.pinimg.com/564x/5c/dc/3c/5cdc3c619f9feed3f5b3e1d34de25db4.jpg',
                'stock' => 100,
                'price' => 15.70,
                'active' => 1,
                'user_id' => 1
            ],
            [
                'name' => 'Banana',
                'description' => 'Banana Chips - Plain - High in Magnesium - (150g)',
                'image' => 'https://www.eatrightbasket.com/wp-content/uploads/2020/10/Banana-Chips-Product.png',
                'stock' => 50,
                'price' => 3.00,
                'active' => 1,
                'user_id' => 1
            ],
            [
                'name' => 'Honey',
                'description' => "PANEL'S Honey 500gm with Dipper",
                'image' => 'https://m.media-amazon.com/images/I/718GWYILDHL._SX522_.jpg',
                'stock' => 3,
                'price' => 20.00,
                'active' => 1,
                'user_id' => 1
            ],
        ];

        foreach ($products as $product) {
            Product::create($product);
        }
    }
}
