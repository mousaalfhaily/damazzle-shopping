<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the product model for a product create, view, update, delete
    | attempt has failed, such as for an invalid info or invalid password.
    |
    */

    'products_list' => 'قائمة بجميع المنتجات.',
    'product_created_successfully' => 'تم انشاء المنتج بنجاح.',
    'data_validation_error' => 'خطأ في التحقق من صحة البيانات.',
    'view_product' => 'عرض معلومات المنتج.',
    'product_updated_successfully' => 'تم تحديث معلومات المنتج بنجاح.',
    'product_was\'t_updated_successfully' => 'لم يتم تحديث معلومات المنتج بنجاح.',
    'product_deleted_successfully' => 'تم حذف المنتج بنجاح.',
    'product_not_found' => 'المنتج غير موجود',
];
