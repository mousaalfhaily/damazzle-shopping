<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'أوراق الاعتماد هذه لا تتطابق مع سجلاتنا.',
    'password' => 'كلمة المرور المقدمة غير صحيحة.',
    'throttle' => 'محاولات تسجيل دخول كثيرة جدًا. يرجى المحاولة مرة أخرى خلال: ثواني.',
    'error_validation' => 'التحقق من صحة الخطأ',
    'user_signed_up_successfully' => 'تم تسجل المستخدم بنجاح',
    'user_signed_in_successfully' => 'تم تسجيل دخول المستخدم بنجاح',
    'email_or_password_are_incorrect' => 'البريد الإلكتروني أو كلمة المرور غير صحيحين',
    'user_signed_out_successfully' => 'تم تسجيل خروج المستخدم بنجاح',
    'forbidden' => 'الوصول ممنول',
    'password_is_incorrect' => 'كلمة المرور غير صحيحة',
    'new_token_created_successfully' => 'تم إنشاء رمز جديد بنجاح',
    'unauthorized' => 'وصول غير مصرح به',
];
