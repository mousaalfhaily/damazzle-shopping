<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during user for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'users_list' => 'قائمة المستخدمين',
    'user_profile_info' => 'معلومات حساب المستخدم',
    'user_updated_successfully' => 'تم تحديث معلومات المستخدم بنجاح.',
    'user_was\'t_updated_successfully' => "لم يتم تحديث معلومات المستخدم بنجاح.",
    'view_user' => 'عرض معلومات المنتج.',
    'user_deleted_successfully' => 'تم حذف المنتج بنجاح.',
    'user_not_found' => 'المنتج غير موجود',
    'can\'t_delete_super_admin' => 'لا يمكن حذف المشرف المتميز',
];
