<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the product model for a product create, view, update, delete
    | attempt has failed, such as for an invalid info or invalid password.
    |
    */

    'products_list' => 'List all products.',
    'product_created_successfully' => 'Product created successfully.',
    'data_validation_error' => 'Data validation error.',
    'view_product' => 'View product.',
    'product_updated_successfully' => 'Product updated successfully.',
    'product_was\'t_updated_successfully' => 'Product was\'t updated successfully.',
    'product_deleted_successfully' => 'Product deleted successfully.',
    'product_not_found' => 'Product not found.',
];
