<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during user for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'users_list' => 'Users list',
    'user_profile_info' => 'User profile info',
    'user_updated_successfully' => 'User updated successfully.',
    'user_was\'t_updated_successfully' => 'User was\'t updated successfully.',
    'view_user' => 'View user.',
    'user_deleted_successfully' => 'User deleted successfully.',
    'user_not_found' => 'User not found.',
    'can\'t_delete_super_admin' => 'Can\'t delete super admin',
];
