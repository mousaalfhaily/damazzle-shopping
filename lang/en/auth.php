<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'error_validation' => 'Error validation',
    'user_signed_up_successfully' => 'Signed up successfully',
    'user_signed_in_successfully' => 'Signed in successfully',
    'email_or_password_are_incorrect' => 'Email or password are incorrect',
    'user_signed_out_successfully' => 'Signed out successfully',
    'forbidden' => 'Access forbidden',
    'password_is_incorrect' => 'Password is incorrect',
    'new_token_created_successfully' => 'New token created successfully',
    'unauthorized' => 'Unauthorized access',
];
