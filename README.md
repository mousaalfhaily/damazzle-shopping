[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://opensource.org/licenses/MIT)

## About DamazzleShopping
-----------------

A website where I show a portfolio of my work and a list of skills and services and accept people's ideas and dreams turning them into real applications.

## Contributing
-----------------

Thank you in advanced if you are considering contributing in DamazzleShopping! To contribute please:

1. Fork the [repo](https://bitbucket.org/mousaalfhaily/damazzle-shopping).

2. Clone it to your machine and set it up.

3. Branch out from `production` and name it like: `{your_name}{-featrue-name-or-title}`.

4. Once done create a pull request to the `'testing'` branch and put me as the reviewer (Mousa Alfhaily).

5. I will review the code and leave some comments if any then I'll approve it, merge it, test it and continue with it to be on the `production` branch and released.

## Security Vulnerabilities
-----------------

If you discover a security vulnerability within DamazzleShopping, please send an e-mail to Mousa Alfhaily via [mousa.alfhaily@gmail.com](mailto:mousa.alfhaily@gmail.com). All security vulnerabilities will be promptly addressed.

## License
-----------------

DamazzleShopping is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Installation & Local Setup
-----------------

#### Requirements

To run this project, you will need

- `PHP 8.0` or higher.

- `Laravel 9`.

- `Composer`.


#### In your terminal

1. Clone the project

```bash
git clone https://mousaalfhaily@bitbucket.org/mousaalfhaily/damazzle-shopping.git
```

2. Go to the project directory

```bash
cd damazzle-shopping
```

3. Install PHP dependencies

```bash
composer install
```

4. Create the `.env` file

```bash
copy .env.example .env
```

5. Generate the project key

```bash
php artisan key:generate
```

6. Create the database empty

```bash
php artisan db:create
```

7. Migrate all database changes and seed it

```bash
php artisan migrate --seed
```

8. Run all the tests

```bash
php artisan test
```

9. Run the project

```bash
php artisan serve
```

10. Open doces in your browser
http://localhost:8000/docs

At this point you should be setup and ready to view, modify and use the project.
