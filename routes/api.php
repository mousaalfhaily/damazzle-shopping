<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::any('/', [AuthController::class, 'abortForbidden']);

Route::middleware("localization")->group(function () {
    Route::prefix('auth')->group(function () {
        // Public routes
        Route::post('/sign-up', [AuthController::class, 'signUp']);
        Route::post('/sign-in', [AuthController::class, 'signIn']);

        // Sanctum auth routes
        Route::middleware("auth:sanctum")->group(function () {
            Route::post('/sign-out', [AuthController::class, 'signOut']);
            Route::get('/me', [AuthController::class, 'myProfile']);
            Route::put('/me', [AuthController::class, 'updateMyProfile']);
            Route::post('/refresh-token', [AuthController::class, 'refreshToken'])->name('refresh.token');
        });
    });

    // With custom Sanctum auth
    Route::apiResource('products', ProductController::class);

    // With full Sanctum auth
    Route::middleware("auth:sanctum")->group(function () {
        Route::apiResource('users', UserController::class);
    });
});
