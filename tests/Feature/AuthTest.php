<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * Test sign up endpoint in case validation errors
     *
     * @return void
     */
    public function test_sign_up_endpoint_validation_errors()
    {
        $this->json('POST', 'api/auth/sign-up')
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(
                [
                    "succcess",
                    "data",
                    "message",
                    "errors",
                ]
            );
    }

    /**
     * Test sign up endpoint in case success
     *
     * @return void
     */
    public function test_sign_up_endpoint_success()
    {
        $result = $this->sign_up();
        $payload = $result['payload'];
        $response = $result['response'];

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "data" => [
                        "user" => ["id"],
                        "token",
                        "refresh_token",
                    ],
                    "message",
                ]
            );

        unset($payload['password'], $payload['password_confirmation']);
        $this->assertDatabaseHas('users', $payload);
    }

    private function sign_up()
    {
        $password = Str::random(8);

        $payload = [
            'name' => $this->faker->firstName,
            'username' => $this->faker->username,
            'email' => $this->faker->email,
            'mobile' => $this->faker->numerify('##########'),
            'password' => $password,
            'password_confirmation' => $password,
        ];

        return [
            'payload' => $payload,
            'response' => $this->json('POST', 'api/auth/sign-up', $payload)
        ];
    }

    /**
     * Test sign is endpoint in case validation errors
     *
     * @return void
     */
    public function test_sign_in_endpoint_validation_errors()
    {
        $this->json('POST', 'api/auth/sign-in')
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(
                [
                    "succcess",
                    "data",
                    "message",
                    "errors",
                ]
            );
    }

    /**
     * Test sign is endpoint in case failure
     *
     * @return void
     */
    public function test_sign_in_endpoint_failure()
    {
        $payload = [
            'email' => 'wrong@email.com',
            'password' => 'wrong-password'
        ];

        $this->json('POST', 'api/auth/sign-in', $payload)
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(
                [
                    "succcess",
                    "data",
                    "message",
                ]
            );
    }

    /**
     * Test sign is endpoint in case success
     *
     * @return void
     */
    public function test_sign_in_endpoint_success()
    {
        $payload = [
            'email' => config('auth.admin_email'),
            'password' => config('auth.admin_password')
        ];

        $this->json('POST', 'api/auth/sign-in', $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "data" => [
                        "user" => ["id"],
                        "token",
                        "refresh_token",
                    ],
                    "message",
                ]
            );
    }

    /**
     * Test sign out endpoint in case failure
     *
     * @return void
     */
    public function test_sign_out_endpoint_failure()
    {
        $this->json('POST', 'api/auth/sign-out')
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJsonStructure(
                [
                    "message"
                ]
            );
    }

    /**
     * Test sign out endpoint in case success
     *
     * @return void
     */
    public function test_sign_out_endpoint_success()
    {
        $result = $this->sign_up();
        $response = $result['response'];
        $json_res = json_decode($response->getContent());
        $token = $json_res->data->token;

        $headers = [
            'Authorization' => "Bearer $token"
        ];

        $this->withHeaders($headers)->json('POST', 'api/auth/sign-out')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "success",
                    "message",
                ]
            );
    }

    /**
     * Test getting a refresh token and use it to refresh auth token in case success
     *
     * @return void
     */
    public function test_refreshing_token_success()
    {
        $result = $this->sign_up();
        $response = $result['response'];
        $json_res = json_decode($response->getContent());
        $refresh_token = $json_res->data->refresh_token;

        $headers = [
            'Authorization' => "Bearer $refresh_token"
        ];

        $this->withHeaders($headers)->json('POST', 'api/auth/refresh-token')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "success",
                    "data" => ["token"],
                    "message",
                ]
            );
    }

    /**
     * Test getting a refresh token and use it to refresh auth token in case failure
     *
     * @return void
     */
    public function test_refreshing_token_failure()
    {
        $headers = [
            'Authorization' => "Bearer wrong-token"
        ];

        $this->withHeaders($headers)->json('POST', 'api/auth/refresh-token')
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJsonStructure(
                [
                    "message",
                ]
            );
    }

    /**
     * Test getting a logged in user profile data in case success
     *
     * @return void
     */
    public function test_getting_profile_success()
    {
        $result = $this->sign_up();
        $response = $result['response'];
        $json_res = json_decode($response->getContent());
        $token = $json_res->data->token;

        $headers = [
            'Authorization' => "Bearer $token"
        ];

        $this->withHeaders($headers)->json('GET', 'api/auth/me')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "success",
                    "data" => ["user"],
                    "message",
                ]
            );
    }

    /**
     * Test getting a logged in user profile data in case failure
     *
     * @return void
     */
    public function test_getting_profile_failure()
    {
        $headers = [
            'Authorization' => "Bearer wrong-token"
        ];

        $this->withHeaders($headers)->json('GET', 'api/auth/me')
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJsonStructure(
                [
                    "message",
                ]
            );
    }

    /**
     * Test updating logged in user profile data in case success
     *
     * @return void
     */
    public function test_updating_profile_success()
    {
        $result = $this->sign_up();
        $payload = $result['payload'];
        $response = $result['response'];
        $json_res = json_decode($response->getContent());
        $token = $json_res->data->token;

        $headers = [
            'Authorization' => "Bearer $token"
        ];

        $password = Str::random(8);

        $new_payload = [
            'name' => $this->faker->firstName,
            'username' => $this->faker->username,
            'email' => $this->faker->email,
            'mobile' => $this->faker->numerify('##########'),
            'password' => $payload['password'],
            'new_password' => $password,
            'new_password_confirmation' => $password,
        ];

        $this->withHeaders($headers)->json('PUT', 'api/auth/me', $new_payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    "success",
                    "data" => ["id"],
                    "message",
                ]
            );
    }

    /**
     * Test updating logged in user profile data in case failure
     *
     * @return void
     */
    public function test_updating_profile_failure()
    {
        $result = $this->sign_up();
        $response = $result['response'];
        $json_res = json_decode($response->getContent());
        $token = $json_res->data->token;

        $headers = [
            'Authorization' => "Bearer $token"
        ];

        $password = Str::random(8);

        $payload = [
            'name' => $this->faker->firstName,
            'username' => $this->faker->username,
            'email' => $this->faker->email,
            'mobile' => $this->faker->numerify('##########'),
            'password' => 'wrong-password',
            'new_password' => $password,
            'new_password_confirmation' => $password,
        ];

        $this->withHeaders($headers)->json('PUT', 'api/auth/me', $payload)
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJsonStructure(
                [
                    "message",
                ]
            );
    }
}
